export class StudentModel {
  id: string;
  name: string;
  createdAt: string;
  age: string;
  address: string;
}
