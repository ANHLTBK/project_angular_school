import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LeftComponent } from './left/left.component';
import { CenterComponent } from './center/center.component';
import { RightComponent } from './right/right.component';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { CenterTestComponent } from './center-test/center-test.component';
import { HttpClientModule } from '@angular/common/http';
import { SchoolComponent } from './school/school.component';
import {SchoolService} from './services/school.service';
import {StudentService} from './services/student.service';
import { StudentComponent } from './student/student.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftComponent,
    CenterComponent,
    RightComponent,
    FooterComponent,
    CenterTestComponent,
    SchoolComponent,
    StudentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [
    SchoolService,
    StudentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
