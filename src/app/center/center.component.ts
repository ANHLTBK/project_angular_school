import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-center',
  templateUrl: './center.component.html',
  styleUrls: ['./center.component.css']
})
export class CenterComponent implements OnInit {
  name = '';
  value = 0;
  nameparent = '';
  filterStatus = 'Xem-tat-ca';
  isShow = true;
  arrayResponse = [];
  employees: any[];
  myservices: any[];

  person = [{name: 'anh 1',  id: 1, country: true},
            {name: 'anh 2',  id: 2, country: false},
            {name: 'anh 3',  id: 3, country: true},
            {name: 'anh 4',  id: 4, country: false},
            {name: 'anh 5',  id: 5, country: true}];
  // Khai báo mảng chứa các tháng
  months = ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4' , 'Tháng 5', 'Tháng 6',
            'Tháng 7' , 'Tháng 8' , 'Tháng 9' , 'Tháng 10', 'Tháng 11' , 'Tháng 12'];
  // mang luu ket qua
  arrayData = ['le tuan anh', 'le quang vinh'];
  // mang cac pt
  fruits = ['Apple', 'Banana'];
  pos = this.fruits.indexOf('Banana');
  last = this.fruits[this.pos];

  constructor() { }

  ngOnInit() {
  }
  addItemToArray() {
    this.arrayData.push(this.last);
  }
  callFun($event: any) {
    this.arrayResponse = [];
    // lay gia tri của event (khi nhap tu input)
    const input = $event.target.value;
    // kiem tra xem có phan tu nao = voi input khong
    const indexInput = this.arrayData.indexOf(input);
    // gan phan tu vua tim được vào biến reponse (this.arrayData[indexInput] lay ra pt)
    const response = this.arrayData[indexInput];
    // đẩy vào mang rỗng
    this.arrayResponse.push(response);
  }
  removeperson(id: number) {
    const index = this.person.findIndex(pers => pers.id === id );
    this.person.splice(index, 1);
  }
  getShowStatus(country: boolean) {
    const dkxemtaca = this.filterStatus === 'Xem-tat-ca';
    const dkxemtudanho = this.filterStatus === 'Xem-tu-da-nho' && country;
    const dkxemtuchuanho = this.filterStatus === 'Xem-chua-nho' && !country;
    return dkxemtaca || dkxemtudanho || dkxemtuchuanho;
  }
  // tslint:disable-next-line:ban-types
  banevent(name: String) {
    this.nameparent = 'anh kool';
  }
}
