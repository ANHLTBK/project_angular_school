import { Component, OnInit } from '@angular/core';
import { StudentService} from '../services/student.service';
import {StudentModel} from '../models/student.model';
import {CommonModule} from '@angular/common';


@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  providers: [StudentService],
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  studentModel: StudentModel = new StudentModel();
  studentsModel: StudentModel[] = [];
  constructor(private studentList: StudentService) { }

  ngOnInit() {
    debugger;
    this.studentsModel = [];
    debugger;
    this.studentModel = new StudentModel();
    debugger;
    this.getAll();
  }
  /* action */
  resetForm() {
    this.studentModel = new StudentModel();
  }
  getByidStudent(id: string) {
    this.studentList.getById(id).subscribe((res: any) => {
      this.studentModel = res;
    });
  }
  getAll() {
    this.studentList.get().subscribe((res: any) => {
      this.studentsModel = res;
    });
  }
  onAddStudent() {
    debugger;
    this.studentList.post(this.studentModel).subscribe((res: any) => {
      if (res != null) {
        alert('them thanh cong');
        this.resetForm();
        this.getAll();
      }
    });
  }
  onDelete(id: any) {
    const confirmResult = confirm('are you sure ');
    if (confirmResult) {
      this.studentList.delete(id).subscribe(response => {
        if (response) {
          this.getAll();
        }
      });
    }
  }
  onUpdateStudent() {
    debugger;
    this.studentList.put(this.studentModel).subscribe(response => {
      this.studentModel = response;
    });
  }

}
