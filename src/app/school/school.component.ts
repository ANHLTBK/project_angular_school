import {Component, OnInit} from '@angular/core';
import {SchoolService} from '../services/school.service';
import {CommonModule} from '@angular/common';
import {SchoolModel} from '../models/school.model';

@Component({
  selector: 'app-school',
  templateUrl: './school.component.html',
  providers: [SchoolService],
  styleUrls: ['./school.component.css']
})
export class SchoolComponent implements OnInit {

  schoolModel: SchoolModel = new SchoolModel();
  schoolsModel: SchoolModel[] = [];
  school: any;

  constructor(private schoollist: SchoolService) {
  }

  ngOnInit() {
    debugger;
    this.schoolsModel = [];
    this.schoolModel = new SchoolModel();
    this.getAll();
  }

  /* action */

  onAdd() {
    this.schoollist.post(this.schoolModel).subscribe((res: any) => {
      if (res != null) {
        alert('them thanh cong');
        this.resetForm();
        this.getAll();
      }
    });
  }


  onDelete(ID: any) {
    // tslint:disable-next-line:quotemark
    const confirmResult = confirm('are you sure ');
    if (confirmResult) {
      this.schoollist.delete(ID).subscribe(response => {
        if (response) {
          this.getAll();
        }
      });
    }
  }

  getAll() {
    this.schoollist.get().subscribe((res: any[]) => {
      this.schoolsModel = res;
    });
  }

  getById(id: string) {
    this.schoollist.getById(id).subscribe((res: any) => {
      this.schoolModel = res;
    });
  }

  onUpdate() {
    this.schoollist.put(this.schoolModel).subscribe((res: any[]) => {
      this.schoolsModel = res;
      this.resetForm();
      this.getAll();
    });
  }

  resetForm() {
    this.schoolModel = new SchoolModel();
  }

  /* end */
}
