import { Component, OnInit , Input } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  // tslint:disable-next-line:ban-types
  @Input() name: String;
  // tslint:disable-next-line:ban-types
  @Input() age: Number;
  constructor() { }

  ngOnInit() {
  }

}
