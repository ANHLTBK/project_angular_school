import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {StudentModel} from '../models/student.model';

@Injectable()
export class StudentService {

  private urlApi = 'http://5cc47e893f761f001422d5ec.mockapi.io/api/schools';

  constructor(private httpClient: HttpClient) {
  }

  get(): Observable<StudentModel[]> {
    const response = this.httpClient.get<StudentModel[]>(this.urlApi).pipe();
    return response;
  }

  getById(id: string): Observable<StudentModel> {
    const response = this.httpClient.get<StudentModel>(this.urlApi + '/' + id).pipe();
    return response;
  }

  delete(id: any): Observable<any> {
    return this.httpClient.delete(this.urlApi + '/' + id).pipe();
  }

  put(request: StudentModel): Observable<any> {
    return this.httpClient.put(this.urlApi + '/' + request.id, request).pipe();
  }

  post(request: StudentModel): Observable<any> {
    return this.httpClient.post(this.urlApi, request).pipe();
  }
}
