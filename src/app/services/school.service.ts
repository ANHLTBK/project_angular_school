import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {SchoolModel} from '../models/school.model';

@Injectable()
export class SchoolService {

  private urlApi = 'http://5cc47e893f761f001422d5ec.mockapi.io/api/schools';

  constructor(private httpClient: HttpClient) {
  }

  get(): Observable<SchoolModel[]> {
    const response = this.httpClient.get<SchoolModel[]>(this.urlApi).pipe();
    return response;
  }

  getById(id: string): Observable<SchoolModel> {
    const response = this.httpClient.get<SchoolModel>(this.urlApi + '/' + id).pipe();
    return response;
  }

  delete(id: any): Observable<any> {
    return this.httpClient.delete(this.urlApi + '/' + id).pipe();
  }

  put(request: SchoolModel): Observable<any> {
    return this.httpClient.put(this.urlApi + '/' + request.id, request).pipe();
  }

  post(request: SchoolModel): Observable<any> {
    return this.httpClient.post(this.urlApi, request).pipe();
  }
}
