import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-center-test',
  templateUrl: './center-test.component.html',
  styleUrls: ['./center-test.component.css']
})
export class CenterTestComponent implements OnInit {
  @Output() myClick = new EventEmitter();
  addforparent() {
    this.myClick.emit();
  }
  addNameparent() {
    this.myClick.emit('test output');
  }
  constructor() { }

  ngOnInit() {
  }

}
